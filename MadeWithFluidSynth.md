This page is for links to Music and Audio created by FluidSynth. 

  * <http://www.analogretro.com/StarRangers> - Christian Collins created the music for the iPhone game Star Rangers using mostly FluidSynth as the synthesizer. 
  * <http://bca.free-artists.net> - Bernd Casper church organ models, utilizing Sven Meier's jOrgan with FluidSynth extension. 
  * <http://www.emergentmusics.org/music/2-new-release> - Mark Conway Wirt released 10 tracks titled \"Emergent Musics\" under the Creative Commons Non-Commercial 3.0 license, for which FluidSynth was one of the software programs used. 
  * <http://www.youtube.com/XRayAus> - All X-Ray Oz songs are created with Rosegarden and QSynth (using the FluidR3 soundfont). 
  * <http://suedwestlicht.saar.de> - Some songs created with free software including FluidSynth and Rosegarden, with female vocals. Many instruments come from the FluidR3 [[SoundFont]]. Website is in German.
  * <https://www.crowdsupply.com/cardona-bits/haxophone> - This hackable electronic saxophone uses FluidSynth for sound.
