**SoundFont** is a file format for sample-based instrument sounds. You will need a SoundFont to use FluidSynth. If you are not familiar with them, check out Josh Green's [**Introduction to SoundFonts**](https://freepats.zenvoid.org/sf2/SF2_Intro.txt) and [**Soundfont 2.1 application note**](https://freepats.zenvoid.org/sf2/sfapp21.pdf).

If you just need to play General Midi files, these SoundFonts are known to work well with FluidSynth: 

  * [S. Christian Collins GeneralUser GS](http://www.schristiancollins.com/generaluser.php) - 30 MB 
  * [Fluid (R3) General MIDI SoundFont (GM)](https://packages.debian.org/fluid-soundfont-gm) - 140 MB 

## Soundfont Resources

  * [Polyphone Soundfont Collection](https://www.polyphone-soundfonts.com/download-soundfonts) - A collection of Soundfonts on the Polyphone website
  * [Hammersound](http://www.hammersound.net/) - A nice resource for downloading free SoundFont instrument files.
  * [Magic Sound Font, version 2.0](http://www.personalcopy.com/sfarkfonts1.htm) (68 MB)
  * [Arachno SoundFont, version 1.0](http://www.arachnosoft.com/main/download.php?id=soundfont-sf2) (148MB)
  * [TimGM6mb](http://sourceforge.net/p/mscore/code/HEAD/tree/trunk/mscore/share/sound/TimGM6mb.sf2?format=raw) (6 MB)
  * [MuseScore_General.sf2](ftp://ftp.osuosl.org/pub/musescore/soundfont/MuseScore_General/MuseScore_General.sf2) (208 MB)
  * [Timbres Of Heaven GM_GS_XG_SFX V 3.4](http://midkar.com/soundfonts/) (246 MB)
  * [Sonatina Symphonic Orchestra](http://ftp.osuosl.org/pub/musescore/soundfont/Sonatina_Symphonic_Orchestra_SF2.zip) (503 MB uncompressed)
  * [Aegean Symphonic Orchestra v2.5 universal](https://sites.google.com/view/hed-sounds/aegean-symphonic-orchestra) (350 MB)
  * [Salamander C5 Light](https://sites.google.com/view/hed-sounds/salamander-c5-light) (25 MB)

## SoundFont editors

- Project **SWAMI** by Josh Green (Win, Linux, MacOS), http://www.swamiproject.org/

- **Polyphone** (Win, Linux, MacOS), http://polyphone-soundfonts.com/en/

- **Vienna SoundFont Studio** by Creative Technology Ltd. (Win)

- **Viena** (Win), http://www.synthfont.com/Viena_news.html

- **Alive** Soundfont Editor by Soundfaction (Win)

**Note:** We cannot recommend using Audio Compositor for creating or editing Soundfonts, as it generates files that violate the Soundfont2 spec (specifically the order of generators as defined in section 8.1.2) and are therefore unusable with FluidSynth!

## Conversion Tools

- [CDxtract](http://www.cdxtract.com) by Safta Consulting, Inc.  (Win)

- [ReCycle](http://www.propellerheads.se/products/recycle/) by Propellerhead Software (Win & Mac),

- [Translator](http://www.chickensys.com/translator) by Rubber Chicken Software (Win & Mac),

- [Awave Studio](https://www.fmjsoft.com/awavestudio.html) by FMJ-Software (Win)

- [sf1to2](http://www.ibiblio.org/thammer/HammerSound/localfiles/soundfonts/sf1to2.zip) (Dos)

**Note** that most of the editors mentioned above may also perform conversions to some extend.

## Software SoundFont Synthesizers:

- LiveSynth Pro DXi and Crescendo from LiveUpdate (Win)

- Unity DS-1 from Bitheadz (Win & Mac)

- [QuickTime 5](http://www.apple.com/quicktime/) from Apple (Win & Mac)

- [Logic from eMagic](http://www.emagic.de)

- [SynthFont](http://www.synthfont.com/)

## Developer Resources

The SoundFont format was originally created by Creative Labs and EMU Systems and used in the SoundBlaster AWE 32 and later cards. There are now many other hardware platforms and software synthesizers supporting this format. SoundFont 2.0 and later are open formats and the specification is freely available. 

  * [Wikipedia SoundFont page](http://en.wikipedia.org/wiki/SoundFont) - Good overview of SoundFont format and other resources.
  * [Soundfont 2.1 application note](http://freepats.zenvoid.org/sf2/sfapp21.pdf) - PDF document describing fundamental SoundFont concepts.
    (should be read first as an overview before diving in [SoundFont 2.4 specification](http://freepats.zenvoid.org/sf2/sfspec24.pdf).
  * [SoundFont 2.4 specification](http://freepats.zenvoid.org/sf2/sfspec24.pdf) - PDF document describing SoundFont format technical details.
  * The MIDI Manufacturers Association has a standard called [Downloadable
  Sounds (DLS)](http://www.midi.org/about-midi/dls/abtdls.htm) that closely resembles the Soundfont Specifications.
  * [Creative Labs Developer Documentation](https://web.archive.org/web/20100728160132/http://connect.creativelabs.com/developer/SoundFont/Forms/AllItems.aspx) - Specifications, docs, SF2 test files, etc. (on archive.org, the original is no longer accessible)
  * [Soundfont.com FAQ](https://web.archive.org/web/20070107111507/http://www.soundblaster.com/soundfont/faqs/) (on archive.org, the original is no longer accessible)
  * [[MuseScore's officially inofficial SoundFont 3 extension|SoundFont3Format]]

## FluidSynth's implementation details of the SoundFont 2 spec

The main goal of FluidSynth is to implement SoundFont 2 spec as accurately as possible. However, some minor adjustments have been made which are described here.

1. The default "note velocity to filter cut off" modulator is inconsistently defined by the spec and is therefore [actively disabled by fluidsynth](https://github.com/FluidSynth/fluidsynth/blob/b1fbace6cb29f4e54d445495ff44f527401285d7/src/synth/fluid_mod.c#L399-L431). Generally, this is no problem, as many people feel that musically it doesn't make sense anyway. Therefore, it is usually not missed by users.

2. FluidSynth applies a custom default modulator to every SoundFont in order to handle CC 8 (Balance) correctly. For details, pls. have a look at [this Pull Request](https://github.com/FluidSynth/fluidsynth/pull/317) or the [related discussion on the mailing list](http://lists.nongnu.org/archive/html/fluid-dev/2018-01/msg00006.html).

3. The SoundFont spec allows into link multiple modulators to each other. An [attempt](https://github.com/FluidSynth/fluidsynth/pull/505) was made to implement this. However, during implementation it was discovered that this feature is essentially "under-specified". Due to a lack for real-world use-cases and because it's hard to understand for a SoundFont designer what's going on in a linked modulator chain, implementing this feature was dropped.

4. According to the spec, stereo sample pairs should both use pitch generators from the right channel. This is not implemented at the moment, as we are lacking an example SoundFont file where this issue would really make an audible difference, see [related issue](https://github.com/FluidSynth/fluidsynth/issues/61).

5. The spec makes various constraints to sample loop points and is absolutely clear, that samples should be dropped or ignored when these conditions are not met. On the other hand, those constraints do not consider the effects of loop offset modulators on instrument level, which could potentially turn invalid sample loops into valid loops on instrument level, or turn valid sample loops into invalid ones. This makes the entire sample loop topic yet another "grey-area" in the SoundFont spec. Rather than messing around with incorrect sample loops or even dropping entire samples, FluidSynth will be very permissive on sample loops. It reports any problems it experiences, but generally leaves them as-is.

