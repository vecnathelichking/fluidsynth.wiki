Feel free to contribute or let us know if something is missing or unclear.

## Users

  * About [[SoundFont]]s
  * [[GettingStarted]] with fluidsynth
  * [[ExampleCommandLines]] - running FluidSynth with different configurations.
  * [[UserManual]]
  * [[FluidSettings]]
  * [[FluidFeatures]] - A chart showing implemented and ready to use MIDI messages.
  * [[LowLatency]] - Getting low latency response from FluidSynth for live playback.

## Developers

  * Tips on [[building Fluidsynth|BuildingWithCMake]]
  * [FluidSynth 2.0 API](http://www.fluidsynth.org/api/)
  * [FluidSynth legacy 1.x API](http://www.fluidsynth.org/api-1.x/)
  * [[ChangeLog]]
  * [[NotesForPackagers]]
  * [LADSPA Interface Documentation](https://github.com/FluidSynth/fluidsynth/blob/master/doc/ladspa.md)
  * [[ReleaseCheckList]] (used by the current FluidSynth release manager)
