# Note: Auto-tools build system has been removed!

This page describes building FluidSynth using the deprecated Auto-tools build system. It has been removed in FluidSynth 2.0. Check out how to build [[FluidSynth using CMake|BuildingWithCMake]].

This page will be removed in the future.
