This is a list of MIDI events that fluidsynth handles by default. That "handling" is either given by

* the [[SoundFont]] spec, or
* the General MIDI standard.

While the explanations in the [[SoundFont]] spec are very precise in terms of the intended audio-synthetic articulation, the descriptions in the MIDI standard are very vague when it comes to sound articulation. Thus you cannot expect that fluidsynth behaves in a certain way (which you may know from other synths) just because you send some MIDI CCs.

In order to control those _"underspecifed"_ aspects of the sound synthesis (like ADSR, low pass filter cutoff & resonance, tremolo & vibrato depth, etc.) custom **SoundFont modulators** must be used! The SoundFont spec wants the SoundFont designer to define those modulators in the SoundFont file itself. Doing so will give you great portability between any SF2 compliant synth, i.e. you would get the same sound articulation when sending your custom CCs to any SF2 compliant synth to trigger your custom effects. Unfortunately, defining those modulators must be done for every single instrument or preset in the SoundFont, which can be tiresome and error-prone. Alternatively, you can use fluidsynth's API for manipulating default modulators (see `fluid_synth_add_default_mod()` and `fluid_synth_remove_default_mod()` resp.). This will allow you to insert or remove your own custom modulators, which will then affect all loaded SoundFonts equally. However, this technique is not portable and limited to fluidsynth.

#### Legend

:heavy_check_mark: Implemented according to MIDI or SoundFont spec and usable by default

:white_check_mark: Partially or customly implemented and usable by default

:warning: Requires special setup of fluidsynth to be usable by default

:x: Not handled by default, but usable via custom SoundFont modulators

## MIDI Message Implementation Chart

| MIDI Message   | Implementation Status |
| -------------- | ------------------ |
| NOTE_OFF       | :heavy_check_mark: |
| NOTE_ON        | :heavy_check_mark: |
| CONTROL_CHANGE | :white_check_mark: [See related table below.](#midi-control-change-implementation-chart) |
| MIDI_SET_TEMPO | :heavy_check_mark: |
| PROGRAM_CHANGE | :heavy_check_mark: |
| CHANNEL_PRESSURE | :heavy_check_mark: SF2 default modulator |
| KEY_PRESSURE | :x: |
| PITCH_BEND | :heavy_check_mark: SF2 default modulator |
| MIDI_SYSTEM_RESET | :heavy_check_mark: |

## MIDI Control Change Implementation Chart

Note that unless otherwise documented, CCs are interpreted individually, i.e. as 7-bit values.

| MIDI CC        | Implementation Status |
| -------------- | ------------------ |
| (000) Bank Select       | :heavy_check_mark: Interpretation of MSB and LSB depends on [`synth.midi-bank-select`](http://www.fluidsynth.org/api/fluidsettings.xml#synth.midi-bank-select) |
| (001) Modulation Wheel        | :heavy_check_mark: SF2 default modulator |
| (002) Breath Controller  | :warning: Usable in _breathmode_, see [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf)  |
| (004) Foot Controller | :x: |
| (005) Portamento Time | :heavy_check_mark: MSB and LSB (i.e. 14-bit value!), see [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |
| (006) Data Entry | :heavy_check_mark: MSB and LSB (i.e. 14-bit value!). |
| (007) Channel Volume | :heavy_check_mark: SF2 default modulator |
| (008) Balance | :white_check_mark: *non-standard* default SF2 modulator |
| (010) Pan | :heavy_check_mark: SF2 default modulator |
| (011) Expression | :heavy_check_mark: SF2 default modulator |
| (064) Sustain Pedal | :heavy_check_mark: See [Sostenuto documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/FluidSostenuto-005.pdf) |
| (065) Portamento Switch | :heavy_check_mark: See [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |
| (066) Sostenuto Pedal | :heavy_check_mark: See [Sostenuto documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/FluidSostenuto-005.pdf) |
| (068) Legato Switch | :heavy_check_mark: See [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |
| (072) Sound Controller 3 (default: Release Time) | :x: |
| (073) Sound Controller 4 (default: Attack Time) | :x: |
| (074) Sound Controller 5 (default: Brightness) | :x: |
| (084) Portamento Control (PTC) | :heavy_check_mark: See [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |
| (091) Effects 1 Depth (Reverb Send Level) | :heavy_check_mark: SF2 default modulator |
| (092) Effects 2 Depth (Tremolo Depth) | :x: |
| (093) Effects 3 Depth (Chorus Send Level) | :heavy_check_mark: SF2 default modulator |
| (094) Effects 4 Depth (Celeste (Detune) Depth) | :x: |
| (095) Effects 5 Depth (Phaser Depth) | :x: |
| (098) NRPN LSB | :heavy_check_mark: [See related table below](#nrpn-control-change-implementation-chart) |
| (099) NRPN MSB | :heavy_check_mark: [See related table below](#nrpn-control-change-implementation-chart) |
| (100) RPN LSB | :white_check_mark: [See related table below](#rpn-control-change-implementation-chart) |
| (101) RPN MSB | :white_check_mark: [See related table below](#rpn-control-change-implementation-chart)
| (120) All Sound Off | :heavy_check_mark: |
| (121) Reset All Controllers | :heavy_check_mark: |
| (121) Local Control | :heavy_check_mark: Ignored, because not applicable |
| (123) All Notes Off | :heavy_check_mark: |
| (124) Omni Mode Off | :heavy_check_mark: See [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |
| (125) Omni Mode On | :heavy_check_mark: See [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |
| (126) Mono Mode | :heavy_check_mark: See [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |
| (127) Poly Mode | :heavy_check_mark: See [PolyMono documentation](https://github.com/FluidSynth/fluidsynth/raw/master/doc/polymono/FluidPolyMono-0004.pdf) |

## RPN Control Change Implementation Chart

| RPN CC   | Implementation Status |
| -------------- | ------------------ |
| (000) RPN_PITCH_BEND_RANGE       | :heavy_check_mark: SF2 default modulator |
| (001) RPN_CHANNEL_FINE_TUNE      | :heavy_check_mark: |
| (002) RPN_CHANNEL_COARSE_TUNE    | :heavy_check_mark: |
| (003) Tuning Program Select      | :heavy_check_mark: |
| (004) Tuning Bank Select         | :heavy_check_mark: |
| (005) Modulation Depth Range     | :x: Not yet implemented |
| (006) MPE Configuration Message  | :x: Not yet implemented |

## NRPN Control Change Implementation Chart

All SF2 generators can be altered with NRPN Control Change messages. See section 8.1.2 in the spec. 

## SysEx Messages

**Note:** fluidsynth only processes those SysEx messages, if the "device-id" in the SysEx message matches the `synth.device-id` setting the synth has been initialized with! Broadcast SysEx messages are always processed.

| SysEx   | Implementation Status |
| -------------- | ------------------ |
| MIDI Tuning Standard | :heavy_check_mark: See [`enum midi_sysex_tuning_msg_id`](https://github.com/FluidSynth/fluidsynth/blob/b8fb6c81e1ca27c0bba2f6a0168832214f91d497/src/midi/fluid_midi.h#L195-L207) for supported messages |
| GS DT1      | :white_check_mark: Only rhythm / melodic part selection messages are supported (since fluidsynth 2.2.0, see the "Patch Part parameters section in SC-88Pro/8850 owner's manual") |
| GM/GM2 mode on | :heavy_check_mark: |
| GS reset | :heavy_check_mark: |
| XG reset | :heavy_check_mark: |