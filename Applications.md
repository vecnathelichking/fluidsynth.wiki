### List of applications using FluidSynth

Feel free to add your application to this page if it *uses* FluidSynth. For music or or other audio created *with* FluidSynth, please see the [[MadeWithFluidSynth]] page.

  * [aisings.com](http://aisings.com) - automatic AI based music generation site that pulls influence from example music. (dead link)
  * [ANMP](https://www.github.com/derselbst/ANMP) - multi-channel, loop-able video game music player for audiophiles and nerds
  * [Ardour](https://ardour.org) - DAW, comes bundled with [a-fluidsynth](https://github.com/Ardour/ardour/tree/master/libs/plugins/a-fluidsynth.lv2) LV2 plugin.
  * [Audacious](https://audacious-media-player.org/) - Open source audio player for Linux, BSD derivatives and Microsoft Windows.
  * <https://beatscratch.io> - BeatScratch uses FluidSynth for playback on Android.
  * [Beeano Midi](http://beeanomidi.com) - MIDI player and converter for Windows.
  * [Cydrea Music](https://www.cydrea.com) - Cydrea Music combines A.I. and procedural generation to bring you a catalog of 65 quadrillion songs that you can play, edit, export (audio and MIDI) and use for free for content creation, game development, music production, etc. The free tier gives you 100+ songs everyday, forever.
  * [Denemo](http://www.denemo.org) - front end for [LilyPond](http:///www.lilypond.org) music typesetter.
  * [DOSBox Staging](https://dosbox-staging.github.io/) - x86/DOS emulator.
  * [eplayOrgan](https://midimusic.github.io) - Windows, Mac and Linux multipurpose organ.
  * [exeCute](https://gitlab.com/es20490446e/exeCute) - Opens exe and bat files as if they were native Linux applications.
  * [FluidGUI](http://fluidgui.sourceforge.net/) - graphical front end for FluidSynth by Ken Ellinwood.
  * [FluidPatcher](https://github.com/albedozero/fluidpatcher) - A Python wrapper combining instrument settings, router rules, etc. into patches. Useful for performance or creating unique sound combinations.
  * [FluidPi](https://github.com/MarquisdeGeek/FluidPi) - Headless synth for Raspberry Pi. So minimal it works from console, controlled using a web server written in bash! 
  * [FluidSynth.clap](https://github.com/cannerycoders/fluidsynth.clap) - CLAP plugin for FluidSynth.
  * [Fluidsynth-DSSI](http://sourceforge.net/projects/dssi/files/fluidsynth-dssi/) - DSSI front end for FluidSynth. 
  * [FluidSynthGUI](http://vkiller2k.users.sourceforge.net/devprojects/FluidSynthGUI/main.php) - windows software sampler based on FluidSynth engine. Select your midi input device, assign sound fonts to channels, and play!
  * [FluidSynthPlugin](https://github.com/prof-spock/FluidSynthPlugin) - a VST3 plugin for a DAW and the pedantic FluidSynthFileConverter both using the FluidSynth library producing identical output (with a spectral difference of less than -100dBFS)
  * [Gluid](http://brouits.free.fr/gluid/) - mini FluidSynth front end for the system tray.
  * [Hachiko](https://github.com/duangsuse-valid-projects/Hachiko) - A tool creating pitch timeline from keystroke.
  * [JJazzLab](https://github.com/jjazzboss/JJazzLab) - A complete and open application for automatic backing tracks generation.
  * [jOrgan](http://jorgan.sourceforge.net/) - java virtual organ utilizing FluidSynth for pipe sound generation.  
  * [juicysfplugin](https://github.com/Birch-san/juicysfplugin) - macOS AU/VST plugin + standalone app for rendering MIDI input through soundfont. JUCE framework.  
  * [KMid](http://kmid2.sourceforge.net/) - featured KDE4 MIDI/karaoke player.
  * [LilypondToBandVideoConverter](https://github.com/prof-spock/LilypondToBandVideoConverter): a generator for notation backing track videos from lilypond files orchestrating lilypond, fluidsynth, sox and ffmpeg.
  * [LMMS](http://github.com/LMMS) - free and open-source cross-platform digital audio workstation.
  * [Mechanical Tunes](https://mechtunes.com) - AI generated streaming music
  * [MidiEditor](http://midieditor.sourceforge.net/) - midi editor for Linux. 
  * [MidiGurdy](http://github.com/midigurdy/) - Electronic musical instrument (hurdy-gurdy) using FluidSynth as internal synthesizer
  * [MonAMI](http://monami.sourceforge.net/) - universal sensor framework, with a FluidSynth reporting plugin. 
  * [MusE](https://muse-sequencer.github.io/) - Linux Music Editor by Werner Schweer, a multi-track sequencer and audio application. 
  * [MuseScore](http://musescore.org/) - free music composition &amp; notation software. 
  * [mt32-pi](https://github.com/dwhinham/mt32-pi) - a baremetal kernel for the Raspberry Pi that turns it into a MIDI synthesizer, based on Munt and FluidSynth.
  * [PianoBooster ](http://pianobooster.sourceforge.net/) - MIDI File Player that teaches you how to play the piano. 
  * [Pocket Composer](https://play.google.com/store/apps/details?id=com.own.pcomposer&hl=en&gl=US) - Ful music theory app that help you to compose and create songs from scratch.
  * [pygame](http://www.pygame.org/) - multi media library for the python language. Uses fluidsynth to play midi music, mostly on linux.
  * [Radium](http://users.notam02.no/~kjetism/radium) - music editor inspired by the tracker interface.
  * [QSynth](http://qsynth.sourceforge.net) - Qt front end for FluidSynth brought to you by Rui Capela. 
  * [Quote2Note](https://github.com/llang629/quote2note) - Translates stock quotes into musical notes. 
  * [ScoreDate](https://github.com/shlomiv/ScoreDate) - open source software written in Java that helps musicians to learn music reading and ear training. 
  * [ScummVM](http://www.scummvm.org/) - program which allows you to run certain classic graphical point-and-click adventure games. 
  * [Simutrans](https://www.simutrans.com/en/) - this game is a freeware and open-source transportation simulator. Your goal is to establish a successful transport company. Transport passengers, mail and goods by rail, road, ship, and even air. Interconnect districts, cities, public buildings, industries and tourist attractions by building a transport network.
  * [SoundFont-Midi-Player](https://play.google.com/store/apps/details?id=org.greh.soundfontmidiplayer) - Android MIDI player app with fast switching of SoundFont files. 
  * [Swami](https://github.com/swami/swami) - collection of software projects for editing and sharing instruments by Josh Green. 
  * [The Miditzer](https://miditzer.org/) - computer program for PC's that lets you recreate a Wurlitzer Style 216 theater organ. 
  * [TuxGuitar](http://www.tuxguitar.com.ar/) - multi-track guitar tablature editor and player that supports many tablature file formats. 
  * [ViPiano](https://github.com/Aldrog/harbour-vipiano) - virtual MIDI Piano Keyboard with FluidSynth for Sailfish OS.
  * [VLC](http://www.videolan.org/) - highly portable multimedia player for various audio and video formats. 
  * [VMPK &amp; FluidSynth](https://vmpk.sourceforge.io/n9/) - virtual MIDI Piano Keyboard with FluidSynth for MeeGo Harmattan devices (Nokia N9 and N950).
  * [XunScore](https://www.xunscore.cn/index_en.html) - A music notation software.
  * [Z-Maestro](http://www.z-sys.org/products/zmaestro) - Windows application which aims to be similar to Apple Garageband by Zachary Northrup.

