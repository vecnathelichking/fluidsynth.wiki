# FluidSynth 2.0

This page is dedicated to ideas for inclusion in the FluidSynth 2.0 development branch. Its a work in progress and some of these may not be included during the development cycle. 

**Feel free to edit this page, add any new ideas or opinions.**

## Proposed Features

  * Sample streaming support 
  * Real synchronous stereo and multi-channel audio (voices guaranteed to start at the same time) 
  * Improve voice stealing algorithm 
  * Synthesis verification and test suites. There needs to be an easy way to verify the output and analyze at a sample by sample basis 
  * Add LV2 support 
  * Research bad out of tune sounding audio when using Chorus and/or Reverb and fix it. 
  * OSC support for control of the features in the telnet interface, from scripts or from language environments such as ChucK, SC, PD, CSound, etc. 
  * RTAudio Output Support 
  * Improve the quality of rendered midi by adding a feature similar to \"human playback\" of Finale, that adds small variations in the MIDI. Human Playback is trademark, so another name should be used here! 

## Documentation

Design ideas, proposals and documentation. 

