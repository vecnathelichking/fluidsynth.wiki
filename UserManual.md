# User Manual

This document has the same layout as the manpage, but it contains links to other pages for more information: [[FluidFeatures]] and [[FluidSettings]].

## NAME

FluidSynth - a [[SoundFont]] synthesizer 

## SYNOPSIS

fluidsynth [options] [ [[SoundFonts|SoundFont]] ] [ midifiles ]

## DESCRIPTION

FluidSynth is a real-time MIDI synthesizer based on the [[SoundFont]]&reg; 2 specifications. It can be used to render MIDI input or MIDI files to audio. The MIDI events are read from a MIDI device. The sound is rendered in real-time to the sound output device. See [[FluidFeatures]] for a comprehensive list of features implemented and working. 

The easiest way to start the synthesizer is to give it a [[SoundFont]] on the command line: `fluidsynth output.sf2`. FluidSynth will load the [[SoundFont]] and read MIDI events from the default MIDI device using the default MIDI driver. Once FluidSynth is running, it reads commands from stdin. There are commands to send MIDI events manually, to load or unload [[SoundFonts|SoundFont]], and so forth. Available commands are [discussed below](#shell-commands). 

FluidSynth can also be used to play a list of MIDI files. Simply run FluidSynth with the [[SoundFont]] and the list of MIDI files to play. In this case, you might not want to open the MIDI device to read external events. Use the `-n` option to deactivate MIDI input. You may also want to deactivate the shell causing FluidSynth to quit as soon as all MIDI files have been played. Start FluidSynth with the `-i` option to do so:

```shell
fluidsynth -ni soundfont.sf2 midifile1.mid midifile2.mid
``` 

Run FluidSynth with the `--help` option to check for changes in the list of options. 

For some scenarios, it might be useful to execute certain shell commands right upon starting FluidSynth, e.g. to do some custom default initialization of the synth, change the default audio driver in use, etc. This can be achieved with a configuration file as described below. If no such file is provided via the `-f` command-line argument, FluidSynth tries to load a user-specific configuration file (as given by [fluid_get_userconf()](https://www.fluidsynth.org/api/group__command__interface.html)). If that fails, it tries to load a system-wide configuration file (as given by [fluid_get_sysconf()](https://www.fluidsynth.org/api/group__command__interface.html)).

## OPTIONS

FluidSynth accepts the following options (call FluidSynth with the --help option to get most up-to-date information): 

-a, --audio-driver=[label]
    

> The audio driver to use. \"-a help\" to list valid options 

-C, --chorus
    

> Turn the chorus on or off [0|1|yes|no, default = on] 

-c, --audio-bufcount=[count]
    

> Number of audio buffers 

-d, --dump
    

> Dump incoming and outgoing MIDI events to stdout 

-E, --audio-file-endian
    

> Audio file endian for fast rendering or aufile driver (\"-E help\" for list) 

-f, --load-config
    

> Right upon starting, load and execute a configuration file containing fluidsynth related shell commands as described [in the Section below](https://github.com/FluidSynth/fluidsynth/wiki/UserManual#shell-commands). 

-F, --fast-render=[file]
    

> Render MIDI file to raw audio data and store in [file] 

-G, --audio-groups
    

> Defines the number of LADSPA audio nodes 

-g, --gain
    

> Set the master gain [0 &lt; gain &lt; 10, default = 0.2] 

-h, --help
    

> Print out this help summary 

-i, --no-shell
    

> Don't read commands from the shell [default = yes] 

-j, --connect-jack-outputs
    

> Attempt to connect the jack outputs to the physical ports 

-K, --midi-channels=[num]
    

> The number of midi channels [default = 16] 

-L, --audio-channels=[num]
    

> The number of stereo audio channels [default = 1] 

-l, --disable-lash
    

> Don't connect to LASH server 

-m, --midi-driver=[label]
    

> The name of the midi driver to use [oss,alsa,alsa_seq,...] 

-n, --no-midi-in
    

> Don't create a midi driver to read MIDI input events [default = yes] 

-O, --audio-file-format
    

> Audio file format for fast rendering or aufile driver (\"-O help\" for list) 

-o
    

> Define a setting, -o name=value (\"-o help\" to dump current values). See [[FluidSettings]] for details 

-p, --portname=[label]

> Set MIDI port name (alsa_seq, coremidi drivers) 

-q, --quiet

> Do not print welcome message or other informational output. (Windows only: also suppress all log messages lower than PANIC)


-R, --reverb
    

> Turn the reverb on or off [0|1|yes|no, default = on] 

-r, --sample-rate
    

> Set the sample rate 

-s, --server
    

> Start FluidSynth as a server process 

-T, --audio-file-type
    

> Audio file type for fast rendering or aufile driver (\"-T help\" for list) 

-v, --verbose
    

> Print out verbose messages about midi events (synth.verbose=1) as well as other debug messages

-V, --version
    

> Show version of program 

-z, --audio-bufsize=[size]
    

> Size of each audio buffer 

## SETTINGS

All the settings that can be passed with the `-o` flag to FluidSynth are known as [[FluidSettings]]. Refer to the wiki page for more details.

## SHELL COMMANDS

When starting FluidSynth an interactive shell opens. This section describes the most common commands.

### GENERAL

help
    

> Prints out a summary of the main commands 

help help
    

> Prints out list of other help topics (type \"help &lt;topic&gt;\") 

quit
    

> Quit the synthesizer 

### SOUNDFONTS

load filename
    

> Load a [[SoundFont]]

unload number
    

> Unload a [[SoundFont]]. The number is the index of the [[SoundFont]] on the stack. 

fonts
    

> Lists the current [[SoundFonts|SoundFont]] on the stack 

inst number
    

> Print out the available instruments for the [[SoundFont]]. 

### MIDI MESSAGES

noteon channel key velocity
    

> Send a note-on event 

noteoff channel key
    

> Send a note-off event 

cc channel ctrl value
    

> Send a control change event 

prog chan num
    

> Send program-change message 

select chan sfont bank prog
    

> Combination of bank-select and program-change 

channels
    

> Print out the presets of all channels. 

### AUDIO SYNTHESIS

gain value
    

> Set the master gain (0 &lt; gain &lt; 5) 

interp num
    

> Choose the interpolation method for all channels 

interpc chan num
    

> Choose the interpolation method for one channel 

### REVERB

*Note:* Before FluidSynth 2.0 custom reverb commands existed. Starting with 2.0 users are encouraged to set reverb parameters via the realtime [[FluidSettings]] as described below.

set synth.reverb.active [0|1]

> Turn the reverb on or off 


set synth.reverb.room-size num

> Change reverb room size (i.e the reverb time) in the range [0 to 1.0] (default: 0.2) 

set synth.reverb.damp num
    
> Change reverb damping in the range [0.0 to 1.0] (default: 0.0)
> * When 0.0, no damping.
> * Between 0.0 and 1.0, higher frequencies have less reverb time than lower frequencies.
> * When 1.0, all frequencies are damped even if room size is at maximum value.

set synth.reverb.width num
    
> Change reverb width in the range [0.0 to 100.0] (default: 0.5)

> num value defines how much the right channel output is separated of the left channel output.
> * When 0.0, there is no separation (i.e the output is mono).
> * When 100.0, the stereo effect is maximum.

set synth.reverb.level num
    
> Change reverb output level in the range [0.0 to 1.0] (default: 0.9)

### CHORUS

*Note:* Before FluidSynth 2.0 custom chorus commands existed. Starting with 2.0 users are encouraged to set chorus parameters via the realtime [[FluidSettings]] as described below.

set synth.chorus.active [0|1]

> Turn the chorus on or off

set synth.chorus.nr n

> Use n delay lines (default 3)

set synth.chorus.level num

> Set output level of each chorus line to num 

set synth.chorus.speed num

> Set mod speed of chorus to num (Hz) 

set synth.chorus.depth num

> Set chorus modulation depth to num (ms) 

### MIDI ROUTER

router_default
    

> Reloads the default MIDI routing rules (input channels are mapped 1:1 to the synth) 

router_clear
    

> Deletes all MIDI routing rules. Please note that deleting all rules means that all events for all types are dropped. Please see the examples below on how to modify only a selected type of event and pass all the rest unchanged.

router_begin [note|cc|prog|pbend|cpress|kpress]
    

> Starts a new routing rule for events of the given type:
> - note - note-on and note-off
> - cc - control change
> - prog - program change
> - pbend - pitch bend
> - cpress - channel pressure (channel aftertouch)
> - kpress - key pressure (polyphonic aftertouch)

router_chan min max mul add
    

> Limits the rule for events on min &lt;= chan &lt;= max. If the channel falls into the window, it is multiplied by 'mul', then 'add' is added. 

router_par1 min max mul add
    

> Limits parameter 1 (for example note number in a note events). Similar to router_chan. 

router_par2 min max mul add
    

> Limits parameter 2 (for example velocity in a note event). Similar to router_chan. 

router_end
    

> Finishes the current rule and adds it to the router. 

#### Router examples

```shell
router_clear  
router_begin note  
router_chan 0 7 0 15  
router_end  
```

Will accept only events of type "note" from the lower 8 MIDI channels, all other events of different types or note events on other channels will be dropped. Regardless of the channel (0-7), the synthesizer plays the note on ch 15 (synthchannel=midichannel*0+15). 

```shell
router_begin cc  
router_chan 0 7 0 15  
router_par1 1 1 0 64  
router_end  
```

Configures the modulation wheel to act as sustain pedal (transforms CC 1 to CC 64 on the lower 8 MIDI channels, routes to ch 15). 

```shell
router_clear

router_begin note
router_par2 0 127 0 60
router_end

router_begin cc
router_end

router_begin prog
router_end
```

This router will only pass note, cc and progam change events. Note events will always have a fixed velocity of 60, cc and prog events are passed on unchanged.


### LADSPA

LADSPA must be enabled for these commands to work (`-o synth.ladspa.active=1`). Please also see the [LADSPA documentation](https://github.com/FluidSynth/fluidsynth/blob/master/doc/ladspa.md) for more details on how to use these commands.

ladspa_effect
> Create a new effect from a LADSPA plugin

ladspa_link
> Connect an effect port to a host port or buffer

ladspa_buffer
> Create a LADSPA buffer

ladspa_set
> Set the value of an effect control port

ladspa_check
> Check LADSPA configuration

ladspa_start
> Start LADSPA effects

ladspa_stop
> Stop LADSPA effect unit

ladspa_reset
> Stop and reset LADSPA effects

## AUTHORS

Peter Hanappe  


Markus Nentwig  


Antoine Schmitt  


Joshua \"Element\" Green  


Stephane Letz  


Please check the [AUTHORS](https://github.com/FluidSynth/fluidsynth/blob/master/AUTHORS) and [THANKS](https://github.com/FluidSynth/fluidsynth/blob/master/THANKS) files for all credits 

## DISCLAIMER

SoundFont® is a registered trademark of Creative Technology Ltd. 
