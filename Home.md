# Welcome to the FluidSynth project Wiki

## About

**FluidSynth** is a free software synthesizer. It's based on the [[SoundFont]] 2 specifications and supports real-time MIDI effect controls. It can be used as a shared library for embedding in other applications, can play MIDI files and has a command-line shell. Many other applications use FluidSynth for audio synthesis.

**Features**

  * Cross-platform support (Linux, macOS and Windows to name a few)
  * [[SoundFont]] 2 support
  * [[SoundFont]] 3 support (vorbis-compressed SF2)
  * Realtime effect modulation using [[SoundFont]] 2.01 modulators
  * Limited support for Downloadable Sounds (DLS) Level 1 & 2
  * Playback of MIDI files
  * Audio files rendering from MIDI files
  * Shared library which can be used in other programs
  * Built-in command line shell

The [Applications](Applications) page has a growing list of other software that uses FluidSynth. 

[[MadeWithFluidSynth]] is a page for users to post links to their Music and Audio creations made with FluidSynth. 

* * *

**DOCUMENTATION**

The [[Documentation]] page contains information for both users and developers on FluidSynth. 

**Applications**

FluidSynth currently has only a command line and shell interface which comes built-in. Most users will likely want to use another application which interfaces to FluidSynth and provides a more friendly GUI. 

The [Applications](Applications) page is a list of other applications that use FluidSynth. 

**LICENSE**

The source code for FluidSynth is distributed under the terms of the [GNU Lesser General Public License](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html). There is also a [[LicensingFAQ]] with a special section about the [Apple App Store](https://github.com/FluidSynth/fluidsynth/wiki/LicensingFAQ#ios-and-the-app-store). 

**MAILING LISTS**

For all questions, support requests and development related discussions regarding FluidSynth, please open a discussion on GitHub or post to our one and only mailing list (developers and users):

[fluid-dev mailing list](http://lists.nongnu.org/mailman/listinfo/fluid-dev)

**Current FluidSynth TEAM**

  * Tom Moebert

**Former FluidSynth TEAM**

  * David Henningsson
  * Element Green
  * Pedro Lopez-Cabanillas

See [AUTHORS](https://github.com/FluidSynth/fluidsynth/blob/master/AUTHORS) file for a list of the many other individuals who have contributed to FluidSynth.
