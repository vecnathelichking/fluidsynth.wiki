### [FluidSynth](Home)
  * [About SoundFont](SoundFont)
  * [Applications using FluidSynth](Applications)
  * [Music made with FluidSynth](MadeWithFluidSynth)
  * [[LicensingFAQ]]

### Get FluidSynth
  * [[Download]]
    * [Using prebuilt libraries on Android](Using-prebuilt-libraries-on-Android)
  * [Build from source](BuildingWithCMake)
  * [Build for Android](BuildingForAndroid)

### [[Documentation]]
  * #### For users
    * [[GettingStarted]]
    * [[ExampleCommandLines]]
    * [[Audio Drivers]]
    * [[UserManual]]
    * [[FluidSettings]]
    * [[FluidFeatures]]
    * [[LowLatency]]

  * #### For Developers
    * [[ChangeLog]]
    * [FluidSynth 2.0 API](http://www.fluidsynth.org/api/)
    * [FluidSynth legacy 1.x API](http://www.fluidsynth.org/api-1.x/)
    * [[SoundFont3Format]]
    * [[NotesForPackagers]]
    * [LADSPA Interface Documentation](https://github.com/FluidSynth/fluidsynth/blob/master/doc/ladspa.md)
    * [[Future]]
    * [[ReleaseCheckList]]